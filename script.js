var animation = document.querySelector(".preloader");
animation.children[0].addEventListener("animationend", function () {
    animation.children[0].classList.remove("ld");
    animation.children[0].classList.add("ld-redy");
    animation.children[1].classList.add("ld");
});
animation.children[1].addEventListener("animationend", function () {
    animation.children[1].classList.remove("ld");
    animation.children[1].classList.add("ld-redy");
    animation.children[2].classList.add("ld");
});
animation.children[2].addEventListener("animationend", function () {
    animation.children[0].classList.remove("ld-redy");
    animation.children[1].classList.remove("ld-redy");
    animation.children[2].classList.remove("ld");
    animation.children[0].classList.add("ld");
});

var email = document.querySelector(".email");
var pass = document.querySelector(".password");
var button = document.querySelector(".button");
var preloader = document.querySelector(".preloader");
var error = document.querySelector(".error");
var form = document.querySelector(".form");
var user = document.querySelector(".user_profile");
var button_reset = document.querySelector(".user_button");
button_reset.onclick = function () {
    user.style.display = "none";
    form.style.display = "block";
}
button.onclick = function () {
    var xhr = new XMLHttpRequest();
    var body = "email=" + email.value + "&password=" + pass.value;
    xhr.open("POST", "http://netology-hj-ajax.herokuapp.com/homework/login_json", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.addEventListener("loadstart", function () {
        form.style.display = "none";
        preloader.style.display = "flex";
    });
    xhr.addEventListener("error", function () {
        form.style.display = "none";
        error.style.display = "block";
    });
    xhr.addEventListener("load", function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            user_data = JSON.parse(xhr.responseText);
            preloader.style.display = "none";
            user.style.display = "block";
            user.children[0].setAttribute("src", user_data.userpic);
            user.children[1].innerText = "User name = " + user_data.lastname + " " + user_data.name + ";";
            user.children[2].innerText = "Country = " + user_data.country + ";";
            user.children[3].innerText = "Hobbies = " + user_data.hobbies[0] + ", " + user_data.hobbies[1] + ", " + user_data.hobbies[2] + ", " + user_data.hobbies[3] + ";";
        }
        else {
            form.style.display = "none";
            error.style.display = "block";
        }
    });
    xhr.send(body);
};

